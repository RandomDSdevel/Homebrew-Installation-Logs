2018-05-20 19:55:52 -0400

cmake
.
-DBUILD_SHARED_LIBS=OFF
-DCMAKE_C_FLAGS_RELEASE=-DNDEBUG
-DCMAKE_CXX_FLAGS_RELEASE=-DNDEBUG
-DCMAKE_INSTALL_PREFIX=/usr/local/Cellar/poppler/0.65.0
-DCMAKE_BUILD_TYPE=Release
-DCMAKE_FIND_FRAMEWORK=LAST
-DCMAKE_VERBOSE_MAKEFILE=ON
-Wno-dev
-DHAVE_CLOCK_GETTIME:INTERNAL=0
-DENABLE_XPDF_HEADERS=ON
-DENABLE_GLIB=ON
-DBUILD_GTK_TESTS=OFF
-DWITH_GObjectIntrospection=ON
-DENABLE_QT4=OFF
-DENABLE_QT5=ON
-DENABLE_CMS=lcms2

CMake Warning at CMakeLists.txt:73 (message):
  

   No test data found in $testdatadir.
   You will not be able to run 'make test' successfully.

  

   The test data is not included in the source packages
   and is also not part of the main git repository. Instead,
   you can checkout the test data from its own git
   repository with:

  

     git clone git://git.freedesktop.org/git/poppler/test

  

   You should checkout the test data as a sibling of your
   poppler source folder or specify the location of your
   checkout with -DTESTDATADIR=/path/to/checkoutdir/test.
    


-- Checking for module 'nss>=3.19'
--   No package 'nss' found
-- Could NOT find NSS3 (missing: NSS3_LIBRARIES NSS3_CFLAGS) 
-- Checking for module 'cairo>=1.10.0'
--   Package 'xcb-shm', required by 'cairo', not found
-- Checking for module 'libopenjp2'
--   Found libopenjp2, version 2.3.0
-- Found lcms version 2.09, /usr/local/lib/liblcms2.dylib
-- Checking for module 'poppler-data'
--   No package 'poppler-data' found
Building Poppler with support for:
  font configuration:   fontconfig
  splash output:        yes
  cairo output:         no
  qt5 wrapper:          yes
  glib wrapper:         no
    introspection:      no
    gtk-doc:            no
  cpp wrapper:          yes
  use libjpeg:          yes
  use libpng:           yes
  use libtiff:          yes
  use zlib compress:    yes
  use zlib uncompress:  no
  use nss3:             no
  use curl:             yes
  use libopenjpeg2:     yes
  use lcms2:            yes
  command line utils:   yes
  test data dir:        /tmp/poppler-20180520-19334-1r1x084/poppler-0.65.0/../test
-- Configuring done
-- Generating done
-- Build files have been written to: /tmp/poppler-20180520-19334-1r1x084/poppler-0.65.0
