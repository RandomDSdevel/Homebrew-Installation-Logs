2018-05-20 19:31:21 -0400

cmake
.
-DCMAKE_C_FLAGS_RELEASE=-DNDEBUG
-DCMAKE_CXX_FLAGS_RELEASE=-DNDEBUG
-DCMAKE_INSTALL_PREFIX=/usr/local/Cellar/poppler/0.65.0
-DCMAKE_BUILD_TYPE=Release
-DCMAKE_FIND_FRAMEWORK=LAST
-DCMAKE_VERBOSE_MAKEFILE=ON
-Wno-dev
-DHAVE_CLOCK_GETTIME:INTERNAL=0
-DENABLE_XPDF_HEADERS=ON
-DENABLE_GLIB=ON
-DBUILD_GTK_TESTS=OFF
-DWITH_GObjectIntrospection=ON
-DENABLE_QT4=OFF
-DENABLE_QT5=ON
-DENABLE_CMS=lcms2

-- The C compiler identification is AppleClang 8.0.0.8000042
-- The CXX compiler identification is AppleClang 8.0.0.8000042
-- Check for working C compiler: /usr/local/Homebrew/Library/Homebrew/shims/super/clang
-- Check for working C compiler: /usr/local/Homebrew/Library/Homebrew/shims/super/clang -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/local/Homebrew/Library/Homebrew/shims/super/clang++
-- Check for working CXX compiler: /usr/local/Homebrew/Library/Homebrew/shims/super/clang++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Found PkgConfig: /usr/local/bin/pkg-config (found version "0.29.2") 
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Looking for pthread_create
-- Looking for pthread_create - found
-- Found Threads: TRUE  
-- Check if the system is big endian
-- Searching 16 bit integer
-- Looking for sys/types.h
-- Looking for sys/types.h - found
-- Looking for stdint.h
-- Looking for stdint.h - found
-- Looking for stddef.h
-- Looking for stddef.h - found
-- Check size of unsigned short
-- Check size of unsigned short - done
-- Using unsigned short
-- Check if the system is big endian - little endian
-- Checking _FILE_OFFSET_BITS for large files
-- Checking _FILE_OFFSET_BITS for large files - not needed
CMake Warning at CMakeLists.txt:73 (message):
  

   No test data found in $testdatadir.
   You will not be able to run 'make test' successfully.

  

   The test data is not included in the source packages
   and is also not part of the main git repository. Instead,
   you can checkout the test data from its own git
   repository with:

  

     git clone git://git.freedesktop.org/git/poppler/test

  

   You should checkout the test data as a sibling of your
   poppler source folder or specify the location of your
   checkout with -DTESTDATADIR=/path/to/checkoutdir/test.
    


-- Found Freetype: /usr/local/lib/libfreetype.dylib (found version "2.9.1") 
-- Checking for module 'fontconfig'
--   Found fontconfig, version 2.13.0
-- Found Fontconfig: /usr/local/lib/libfontconfig.dylib  
-- Found JPEG: /usr/local/lib/libjpeg.dylib  
-- Found ZLIB: /usr/lib/libz.dylib (found version "1.2.5") 
-- Found PNG: /usr/local/lib/libpng.dylib (found version "1.6.34") 
-- Found TIFF: /usr/local/lib/libtiff.dylib (found version "4.0.9") 
-- Checking for module 'nss>=3.19'
--   No package 'nss' found
-- Could NOT find NSS3 (missing: NSS3_LIBRARIES NSS3_CFLAGS) 
-- Checking for module 'cairo>=1.10.0'
--   Package 'xcb-shm', required by 'cairo', not found
-- Performing Test ICONV_SECOND_ARGUMENT_IS_CONST
-- Performing Test ICONV_SECOND_ARGUMENT_IS_CONST - Failed
-- Found Iconv: /usr/lib/libiconv.dylib
-- Checking for module 'libopenjp2'
--   Found libopenjp2, version 2.3.0
-- Checking for module 'lcms2'
--   Found lcms2, version 2.9
-- Found lcms version 2.09, /usr/local/lib/liblcms2.dylib
-- Found CURL: /usr/lib/libcurl.dylib (found version "7.43.0") 
-- Checking for module 'poppler-data'
--   No package 'poppler-data' found
-- Looking for include file dlfcn.h
-- Looking for include file dlfcn.h - found
-- Looking for include file fcntl.h
-- Looking for include file fcntl.h - found
-- Looking for include file stdlib.h
-- Looking for include file stdlib.h - found
-- Looking for include file sys/mman.h
-- Looking for include file sys/mman.h - found
-- Looking for include file sys/stat.h
-- Looking for include file sys/stat.h - found
-- Looking for include file unistd.h
-- Looking for include file unistd.h - found
-- Looking for fseek64
-- Looking for fseek64 - not found
-- Looking for fseeko
-- Looking for fseeko - found
-- Looking for ftell64
-- Looking for ftell64 - not found
-- Looking for pread64
-- Looking for pread64 - not found
-- Looking for lseek64
-- Looking for lseek64 - not found
-- Looking for gmtime_r
-- Looking for gmtime_r - found
-- Looking for timegm
-- Looking for timegm - found
-- Looking for gettimeofday
-- Looking for gettimeofday - found
-- Looking for localtime_r
-- Looking for localtime_r - found
-- Looking for popen
-- Looking for popen - found
-- Looking for mkstemp
-- Looking for mkstemp - found
-- Looking for rand_r
-- Looking for rand_r - found
-- Looking for strcpy_s
-- Looking for strcpy_s - not found
-- Looking for strcat_s
-- Looking for strcat_s - not found
-- Looking for strtok_r
-- Looking for strtok_r - found
-- Performing Test HAVE_DIRENT_H
-- Performing Test HAVE_DIRENT_H - Success
-- Performing Test HAVE_NDIR_H
-- Performing Test HAVE_NDIR_H - Failed
-- Performing Test HAVE_SYS_DIR_H
-- Performing Test HAVE_SYS_DIR_H - Success
-- Performing Test HAVE_SYS_NDIR_H
-- Performing Test HAVE_SYS_NDIR_H - Failed
-- Looking for nanosleep
-- Looking for nanosleep - found
Building Poppler with support for:
  font configuration:   fontconfig
  splash output:        yes
  cairo output:         no
  qt5 wrapper:          yes
  glib wrapper:         no
    introspection:      no
    gtk-doc:            no
  cpp wrapper:          yes
  use libjpeg:          yes
  use libpng:           yes
  use libtiff:          yes
  use zlib compress:    yes
  use zlib uncompress:  no
  use nss3:             no
  use curl:             yes
  use libopenjpeg2:     yes
  use lcms2:            yes
  command line utils:   yes
  test data dir:        /tmp/poppler-20180520-14848-ww17p0/poppler-0.65.0/../test
-- Configuring done
-- Generating done
CMake Warning:
  Manually-specified variables were not used by the project:

    ENABLE_QT4
    HAVE_CLOCK_GETTIME
    WITH_GObjectIntrospection


-- Build files have been written to: /tmp/poppler-20180520-14848-ww17p0/poppler-0.65.0
